import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class Escrowstep1Service {

    inputGroupSelect1:string
    inputGroupSelect2:string
    inputGroupSelect3:any
    inputGroupSelect4:any
    inputGroupSelect5:any
    inputGroupSelect6:string

  constructor() { }
}
